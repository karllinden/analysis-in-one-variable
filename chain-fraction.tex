\documentclass[a4paper]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}

\begin{document}

\title{A chain fraction}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle

\vfill

\input{title-footer.tex}
\clearpage
\null
\clearpage

\section*{A chain fraction}

\subsection*{Task}

Analyse the chain fraction
\begin{equation}\label{eq:frac}
  \cfrac{1}{1 + \cfrac{1}{1 + \cfrac{1}{1 + \cdots}}}.
\end{equation}

\subsection*{Solution}
The obvious recursion formula is
\begin{equation}\label{eq:f}
  a_{n+1} = f(a_n), \quad f(x) = \frac{1}{1+x}.
\end{equation}
But what is the initial value \(a_0\)? A reasonable \emph{guess} would be 0, or
maybe 1, but from \eqref{eq:frac} it is not clear.
It is actually a matter of how to interpret the ``\(\cdots\)''.
Thus it is necessary to analyse the sequence \((a_n)_{n=0}^\infty\) defined by
\eqref{eq:f} for all possible values of \(a_0\), to find out if \eqref{eq:frac}
makes sense.

\begin{mylem}\label{lem:fibclo}
  If \((F_n)_{n=0}^\infty\) is the Fibonacci sequence,
  \[ F_{n+2} = F_{n+1} + F_n, \quad F_0 = 0, \quad F_1 = 1, \]
  then for all \(n \in \mathbb{N}\)
  \[
    F_n =
      \left( \frac{1+\sqrt{5}}{2} \right)^n -
      \left( \frac{1-\sqrt{5}}{2} \right)^n.
  \]
\end{mylem}
\begin{proof}
  It is known that
  \[
    \begin{pmatrix}
      F_{n+2} \\
      F_{n+1}
    \end{pmatrix}
    =
    \begin{pmatrix}
      F_{n+1} + F_n \\
      F_{n+1}
    \end{pmatrix}
    =
    \begin{pmatrix}
      1 & 1 \\
      1 & 0
    \end{pmatrix}
    \begin{pmatrix}
      F_{n+1} \\
      F_n
    \end{pmatrix}
  \]
  Let
  \(X^{(n)} =
    \begin{pmatrix}
      F_{n+1} \\
      F_n
    \end{pmatrix}\)
  and
  \(A =
    \begin{pmatrix}
      1 & 1 \\
      1 & 0
    \end{pmatrix}\), which
  gives
  \[ X^{(n+1)} = A X^{(n)}. \]
  It is easy to realise that
  \begin{equation}\label{eq:xn}
    X^{(n)} = A^n X^{(0)}.
  \end{equation}
  To simplify this one can use the fact that the matrix \(A\) is diagonizable.
  To find the eigenvalues one does
  \[ AX = \lambda X \iff (A-\lambda I)X = 0. \]
  This equation has non-trivial solutions if and only if
  \(\det{(A-\lambda I)} = 0\), which give the eigenvalues
  \[
    \begin{split}
      (1-\lambda)(-\lambda) - 1 = 0
        &\iff \lambda^2 - \lambda - 1 = 0 \\
        &\iff (\lambda-\frac{1}{2})^2 - \frac{5}{4} = 0 \\
        &\iff \lambda = \frac{1 \pm \sqrt{5}}{2} \\
        &\iff \lambda_1 = \frac{1 + \sqrt{5}}{2}, \;
              \lambda_2 = \frac{1 - \sqrt{5}}{2}.
    \end{split}
  \]
  A pair of corresponding eigenvectors are
  \(X_1 =
    \begin{pmatrix}
      \frac{1+\sqrt{5}}{2} \\
      1
    \end{pmatrix}\)
  and
  \(X_2 =
    \begin{pmatrix}
      \frac{1-\sqrt{5}}{2} \\
      1
    \end{pmatrix}\).
  Since \(AX_1 = \lambda_1 X_1\) and \(AX_2 = \lambda_2 X_2\) it is known that
  \[
    A
    \begin{pmatrix}
      X_1 & X_2
    \end{pmatrix}
    =
    \begin{pmatrix}
      \lambda_1 X_1 & \lambda_2 X_2
    \end{pmatrix}
    =
    \begin{pmatrix}
      X_1 & X_2
    \end{pmatrix}
    \begin{pmatrix}
      \lambda_1 & 0 \\
      0 & \lambda_2
    \end{pmatrix}.
  \]
  Let
  \(T =
    \begin{pmatrix}
      X_1 & X_2
    \end{pmatrix}\)
  and
  \(D =
    \begin{pmatrix}
      \lambda_1 & 0 \\
      0 & \lambda_2
    \end{pmatrix}\).
  This yields
  \[ AT = TD \iff A = T D T^{-1}. \]
  Thus the matrix \(A\) is diagonalized by
  \(T =
    \begin{pmatrix}
      \frac{1+\sqrt{5}}{2} & \frac{1-\sqrt{5}}{2} \\
      1 & 1
    \end{pmatrix}\)
  and its inverse
  \(T^{-1} =
    \frac{1}{\sqrt{5}}
    \begin{pmatrix}
      1 & \frac{\sqrt{5}-1}{2} \\
      -1 & \frac{\sqrt{5}+1}{2}
    \end{pmatrix}\).
  Using this, \eqref{eq:xn} can be simplified to
  \[ X^{(n)} = A^n X^{(0)} = (T D T^{-1})^n X^{(0)} = T D^n T^{-1} X^{(0)}. \]
  Writing the matrices out, this is acquired:
  \[
    \begin{pmatrix}
      F_{n+1} \\
      F_n
    \end{pmatrix}
    =
    \begin{pmatrix}
      \frac{1+\sqrt{5}}{2} & \frac{1-\sqrt{5}}{2} \\
      1 & 1
    \end{pmatrix}
    \begin{pmatrix}
      \left(\frac{1+\sqrt{5}}{2}\right)^n & 0 \\
      0 & \left(\frac{1-\sqrt{5}}{2}\right)^n
    \end{pmatrix}
    \frac{1}{\sqrt{5}}
    \begin{pmatrix}
      1 & \frac{\sqrt{5}-1}{2} \\
      -1 & \frac{\sqrt{5}+1}{2}
    \end{pmatrix}
    \begin{pmatrix}
      F_1 \\
      F_0
    \end{pmatrix}.
  \]
  Knowing that \(F_1 = 1\) and \(F_0 = 0\), then the right-hand expression can
  be simplified like this:
  \[
    \begin{split}
      \begin{pmatrix}
        F_{n+1} \\
        F_n
      \end{pmatrix}
      &=
      \frac{1}{\sqrt{5}}
      \begin{pmatrix}
        \frac{1+\sqrt{5}}{2} & \frac{1-\sqrt{5}}{2} \\
        1 & 1
      \end{pmatrix}
      \begin{pmatrix}
        \left(\frac{1+\sqrt{5}}{2}\right)^n & 0 \\
        0 & \left(\frac{1-\sqrt{5}}{2}\right)^n
      \end{pmatrix}
      \begin{pmatrix}
        1 & \frac{\sqrt{5}-1}{2} \\
        -1 & \frac{\sqrt{5}+1}{2}
      \end{pmatrix}
      \begin{pmatrix}
        1 \\
        0
      \end{pmatrix} \\
      &=
      \frac{1}{\sqrt{5}}
      \begin{pmatrix}
        \frac{1+\sqrt{5}}{2} & \frac{1-\sqrt{5}}{2} \\
        1 & 1
      \end{pmatrix}
      \begin{pmatrix}
        \left(\frac{1+\sqrt{5}}{2}\right)^n & 0 \\
        0 & \left(\frac{1-\sqrt{5}}{2}\right)^n
      \end{pmatrix}
      \begin{pmatrix}
        1 \\
        -1
      \end{pmatrix} \\
      &=
      \frac{1}{\sqrt{5}}
      \begin{pmatrix}
        \frac{1+\sqrt{5}}{2} & \frac{1-\sqrt{5}}{2} \\
        1 & 1
      \end{pmatrix}
      \begin{pmatrix}
        \left(\frac{1+\sqrt{5}}{2}\right)^n \\
        -\left(\frac{1-\sqrt{5}}{2}\right)^n
      \end{pmatrix} \\
      &=
      \frac{1}{\sqrt{5}}
      \begin{pmatrix}
        \left(\frac{1+\sqrt{5}}{2}\right)^{n+1}
          - \left(\frac{1-\sqrt{5}}{2}\right)^{n+1} \\
        \left(\frac{1+\sqrt{5}}{2}\right)^n
          - \left(\frac{1-\sqrt{5}}{2}\right)^n \\
      \end{pmatrix}
    \end{split}
  \]
  It now follows directly that
  \[
    F_n =
      \left( \frac{1+\sqrt{5}}{2} \right)^n -
      \left( \frac{1-\sqrt{5}}{2} \right)^n,
  \]
  which is the sought result.
\end{proof}

\begin{mylem}\label{lem:fiblim}
  Let \((F_n)_{n=0}^\infty\) be as in lemma \ref{lem:fibclo}. Then
  \begin{equation}\label{eq:fiblim1}
    \lim_{n \to \infty} \frac{F_{n+1}}{F_n} = \frac{\sqrt{5}+1}{2}
  \end{equation}
  and
  \begin{equation}\label{eq:fiblim2}
    \lim_{n \to \infty} \frac{F_n}{F_{n+1}} = \frac{\sqrt{5}-1}{2}.
  \end{equation}
\end{mylem}
\begin{proof}
  The following limit will be used to compute both \eqref{eq:fiblim1} and
  \eqref{eq:fiblim2}.
  \begin{equation}\label{eq:fiblim3}
    \lim_{n \to \infty} \left( \frac{1 - \sqrt{5}}{1+\sqrt{5}} \right)^n = 0.
  \end{equation}
  This is true since
  \[
    |1-\sqrt{5}| = \sqrt{5}-1 < \sqrt{5} + 1 < |1+\sqrt{5}| \implies
      \left| \frac{1 - \sqrt{5}}{1+\sqrt{5}} \right| < 1.
  \]

  Begin with \eqref{eq:fiblim1}.
  \[
    \begin{split}
      \lim_{n \to \infty} \frac{F_{n+1}}{F_n}
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{1+\sqrt{5}}{2} \right)^{n+1} -
            \left( \frac{1-\sqrt{5}}{2} \right)^{n+1}
          }{
            \left( \frac{1+\sqrt{5}}{2} \right)^n -
            \left( \frac{1-\sqrt{5}}{2} \right)^n
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{2}{1+\sqrt{5}} \right)^n
            \left[
              \left( \frac{1+\sqrt{5}}{2} \right)^{n+1} -
              \left( \frac{1-\sqrt{5}}{2} \right)^{n+1}
            \right]
          }{
            \left( \frac{2}{1+\sqrt{5}} \right)^n
            \left[
              \left( \frac{1+\sqrt{5}}{2} \right)^n -
              \left( \frac{1-\sqrt{5}}{2} \right)^n
            \right]
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{1+\sqrt{5}}{2} \right) -
            \left( \frac{1-\sqrt{5}}{2} \right)
            \left( \frac{2}{1+\sqrt{5}} \right)^n
            \left( \frac{1-\sqrt{5}}{2} \right)^n
          }{
            1 -
            \left( \frac{2}{1+\sqrt{5}} \right)^n
            \left( \frac{1-\sqrt{5}}{2} \right)^n
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{1+\sqrt{5}}{2} \right) -
            \left( \frac{1-\sqrt{5}}{2} \right)
            \left( \frac{1-\sqrt{5}}{1+\sqrt{5}} \right)^n
          }{
            1 -
            \left( \frac{1-\sqrt{5}}{1+\sqrt{5}} \right)^n
          } \\
        &\overset{\eqref{eq:fiblim3}}{=}
          \frac{
            \left( \frac{1+\sqrt{5}}{2} \right) -
            \left( \frac{1-\sqrt{5}}{2} \right) \cdot 0
          }{1 - 0} \\
        &=
          \frac{\sqrt{5}+1}{2}
    \end{split}
  \]
  This proves \eqref{eq:fiblim1}.

  Next consider \eqref{eq:fiblim2}.
  \[
    \begin{split}
      \lim_{n \to \infty} \frac{F_n}{F_{n+1}}
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{1+\sqrt{5}}{2} \right)^n -
            \left( \frac{1-\sqrt{5}}{2} \right)^n
          }{
            \left( \frac{1+\sqrt{5}}{2} \right)^{n+1} -
            \left( \frac{1-\sqrt{5}}{2} \right)^{n+1}
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{2}{1+\sqrt{5}} \right)^{n+1}
            \left[
              \left( \frac{1+\sqrt{5}}{2} \right)^n -
              \left( \frac{1-\sqrt{5}}{2} \right)^n
            \right]
          }{
            \left( \frac{2}{1+\sqrt{5}} \right)^{n+1}
            \left[
              \left( \frac{1+\sqrt{5}}{2} \right)^{n+1} -
              \left( \frac{1-\sqrt{5}}{2} \right)^{n+1}
            \right]
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{2}{1+\sqrt{5}} \right) -
            \left( \frac{2}{1+\sqrt{5}} \right)
            \left( \frac{2}{1+\sqrt{5}} \right)^n
            \left( \frac{1-\sqrt{5}}{2} \right)^n
          }{
            1 -
            \left( \frac{2}{1+\sqrt{5}} \right)^{n+1}
            \left( \frac{1-\sqrt{5}}{2} \right)^{n+1}
          } \\
        &=
          \lim_{n \to \infty}
          \frac{
            \left( \frac{2}{1+\sqrt{5}} \right) -
            \left( \frac{2}{1+\sqrt{5}} \right)
            \left( \frac{1-\sqrt{5}}{1+\sqrt{5}} \right)^n
          }{
            1 -
            \left( \frac{1-\sqrt{5}}{1+\sqrt{5}} \right)^{n+1}
          } \\
        &\overset{\eqref{eq:fiblim3}}{=}
          \frac{
            \left( \frac{2}{1+\sqrt{5}} \right) -
            \left( \frac{2}{1+\sqrt{5}} \right) \cdot 0
          }{1 - 0}
         = \frac{2}{1+\sqrt{5}} = \frac{2(1-\sqrt{5})}{1-5} \\
        &= \frac{\sqrt{5} - 1}{2}
    \end{split}
  \]
  This proves \eqref{eq:fiblim2}, which fulfills the proof.
\end{proof}

\begin{mythm}\label{thm:a0}
  \(a_0 = 0 \implies \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\).
\end{mythm}
\begin{proof}
  The proof uses the fact that \(a_n\) can be written as
  \(a_n = \frac{p_n}{q_n}\) for all \(n \in \mathbb{N}\).
  This is proved using induction.
  Clearly this is true for \(n = 0\) since \(p_0\) and \(q_0\) can be chosen
  to be 0 and 1 respectively.
  Now assume \(a_k = \frac{p_k}{q_k}\) for some \(k \in \mathbb{N}\).
  This gives
  \[
    a_{k+1} = f(a_k)
      = \frac{1}{1+a_k}
      = \frac{1}{1+\frac{p_k}{q_k}}
      = \frac{q_k}{q_k + p_k}
      = \frac{p_{k+1}}{q_{k+1}}.
  \]
  This together with the induction principle proves that the statement is true
  for all \(n \in \mathbb{N}\).
  Also from the above equality one sees that
  \[ q_{k+2} = q_{k+1} + p_{k+1} = q_{k+1} + q_k. \]
  Since \(q_0 = 0\) and \(q_1 = 1\), the sequence \((q_n)_{n=0}^\infty\) is
  exactly the Fibonacci sequence, thus a more suitable name for the \(q_n\) is
  \(F_n\)
  which yields
  \begin{equation}\label{eq:fibonacci}
    a_n = \frac{F_{n}}{F_{n+1}}, \quad F_{n+2} = F_{n+1} + F_{n},
    \quad F_0 = 0, \quad F_1 = 1.
  \end{equation}
  Lemma \ref{lem:fiblim} now gives the desired result.
\end{proof}

\begin{mylem}\label{lem:ab1}
  \(x \in (a,b) \subset (-1,\infty), \; a \neq -1
  \implies f(x) \in (f(b),f(a))\).
\end{mylem}
\begin{proof}
  \[
    \begin{split}
      a < x
        &\iff 1 + a < 1 + x \\
        &\overset{1+x > 0}{\iff} \frac{1+a}{1+x} < 1 \\
        &\overset{1+a > 0}{\iff} \frac{1}{1+x} < \frac{1}{1+a} \\
        &\iff f(x) < f(a)
    \end{split}
  \]
  and
  \[
    \begin{split}
      b > x
        &\iff 1 + b > 1 + x \\
        &\overset{1+x > 0}{\iff} \frac{1+b}{1+x} > 1 \\
        &\overset{1+b > 0}{\iff} \frac{1}{1+x} > \frac{1}{1+b} \\
        &\iff f(x) > f(b) \\
    \end{split}
  \]
  which yield \(f(b) < f(x) < f(a) \iff f(x) \in (f(b),f(a))\).
\end{proof}

\begin{mylem}\label{lem:ab2}
  \(x \in (a,b) \subset (-\infty,-1), \; b \neq -1
  \implies f(x) \in (f(b),f(a))\).
\end{mylem}
\begin{proof}
  \[
    \begin{split}
      a < x
        &\iff 1 + a < 1 + x \\
        &\overset{1+x < 0}{\iff} \frac{1+a}{1+x} > 1 \\
        &\overset{1+a < 0}{\iff} \frac{1}{1+x} < \frac{1}{1+a} \\
        &\iff f(x) < f(a)
    \end{split}
  \]
  and
  \[
    \begin{split}
      b > x
        &\iff 1 + b > 1 + x \\
        &\overset{1+x < 0}{\iff} \frac{1+b}{1+x} < 1 \\
        &\overset{1+b < 0}{\iff} \frac{1}{1+x} > \frac{1}{1+b} \\
        &\iff f(x) > f(b) \\
    \end{split}
  \]
  which yield \(f(b) < f(x) < f(a) \iff f(x) \in (f(b),f(a))\).
\end{proof}

\begin{mythm}\label{thm:a01}
  \(\exists k \in \mathbb{N} : \; a_k \in (0,1) \implies
   \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\).
\end{mythm}
\begin{proof}
  Let \((b_n)_{n=0}^\infty\) be the sequence with defined by
  \[ b_0 = 0, \quad b_{n+1} = f(b_n). \]
  From this \(b_1 = 1\) can be acquired.
  Thus the condition \(a_k \in (0,1)\) is the same as \(a_k \in (b_0, b_1)\).
  Lemma \ref{lem:ab1} gives \(f(a_k) \in (f(b_1),f(b_0))\), but this is the same
  as \(a_{k+1} \in (b_2,b_1)\).
  One more iteration gives \(a_{k+2} \in (b_2,b_3)\).
  This process can be repeated infinitely.
  Generally it can be proven that \(a_{k+2m} \in (b_{2m},b_{2m+1})\) for all
  \(m \in \mathbb{N}\).
  For \(m = 0\) this is obviously true.
  Suppose the statement is true for \(m = p\), that is
  \(a_{k+2p} \in (b_{2p},b_{2p+1})\), then use of lemma \ref{lem:ab2} gives
  firstly \(a_{k+2p+1} \in (b_{2p+2},b_{2p+1})\) and secondly
  \(a_{k+2p+2} \in (b_{2p+2},b_{2p+3})\) but this is the statement for
  \(m = p + 1\).
  Together with the induction principle this proves that
  \[ \forall m \in \mathbb{N} :\; a_{k+2m} \in (b_{2m},b_{2m+1}). \]
  Since the sequence \((b_n)_{n=0}^\infty\) is the same as
  \((a_n)_{n=0}^\infty\) when \(a_0 = 0\), theorem \ref{thm:a0} gives
  \(\lim_{n \to \infty} b_n = \frac{\sqrt{5}-1}{2}\).
  The fact that \(b_{2m},b_{2m+1} \to \frac{\sqrt{5}-1}{2}\) as
  \(2m \to \infty\) and \(b_{2m} < a_{k+2m} < b_{2m+1}\), gives together with
  the sandwich/squeeze theorem that
  \[
    \lim_{2m \to \infty} a_{k+2m} =
    \lim_{k + 2m \to \infty} a_{k+2m} = \frac{\sqrt{5}-1}{2}.
  \]
  Substituting \(n\) for \(k + 2m\) above gives the desired result.
\end{proof}

\begin{mythm}\label{thm:a0inf}
  \(\exists k \in \mathbb{Z} : a_k \in (0,\infty) \implies
  \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\).
\end{mythm}
\begin{proof}
  \[ a_{k+1} = f(a_k) = \frac{1}{1+a_k} < \frac{1}{1 + 0} = 1. \]
  Also it is obvious that
  \(a_{k+1} = f(a_k) > 0\). Thus
  \[ 0 < a_{k+1} < 1 \iff a_{k+1} \in (0,1). \]
  Theorem \ref{thm:a01} now gives the desired result.
\end{proof}

\begin{mythm}\label{thm:am10}
  \(\exists k \in \mathbb{Z} : a_k \in (-1,0) \implies
  \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\).
\end{mythm}
\begin{proof}
  Using the definition of \(a_{k+1}\),
  \[ a_{k+1} = f(a_k) = \frac{1}{1+a_k}, \]
  it is evident that when \(a_k \in (-1,0)\) both the nominator and the
  denominator are positive.
  Thus \(a_{k+1} > 0\).
  This is the same as \(a_{k+1} \in (0,\infty)\) and the result follows from
  theorem \ref{thm:a0inf}.
\end{proof}

\begin{mythm}\label{thm:aminfm2}
  \(\exists k \in \mathbb{Z} : a_k \in (-\infty,-2) \implies
  \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\)
\end{mythm}
\begin{proof}
  \[
    a_k < -2 \iff 1 + a_k < -1 \iff \frac{1}{1+a_k} > -1
    \iff f(a_k) > -1.
  \]
  Since
  \[ a_{k+1} = f(a_k) = \frac{1}{1+a_k} \]
  and the \(a_k < -2\) the denominator is always negative, which gives
  \(a_{k+1} = f(a_k) < 0\). Together \(a_{k+1} \in (-1,0)\) and the result
  follows from theorem \ref{thm:am10}.
\end{proof}

By the above theorems, it is known that if \(a_0\) is picked from any of the
intervals \((-\infty,-2)\), \((-1,0)\), \([0,0]\), \((0,\infty)\) the sequence
always converges to \(\frac{\sqrt{5}-1}{2}\). But the interval \([-2,-1]\) is
yet to be
analysed. Since \(f\) is not defined for -1, it is impossible to use any \(a_k\)
such that, eventually, \(a_n = -1\). To find out which values eventually yield
\(a_n = -1\), the inverse of \(f\) can be used. Let \(y = f(x)\), then
\[ y = \frac{1}{1+x} \iff 1+x = \frac{1}{y} \iff x = \frac{1-y}{y}. \]
This shows that
\[ f^{-1}(x) = \frac{1-x}{x}. \]
Define the sequence \((d_n)_{n=0}^\infty\) as
\[ d_{n+1} = f^{-1}(d_n), \quad d_0 = -1. \]

It is obvious that if \(a_i = d_j\), for any \(i,j \in \mathbb{N}\), it is
evident that \(a_{i+j} = d_{j-j} = d_0 = -1\), which means the limit is not
defined.
Thus the condition
\[ \forall k \in \mathbb{N} : a_0 \neq d_k \]
must be fulfilled in order for the limit to be defined.

Two lemmas are needed in order fully understand what is going on when
\(a_k \in [-2,-1]\).

\begin{mylem}\label{lem:dlim}
  \(\lim_{n \to \infty} d_n = -\frac{\sqrt{5}+1}{2}.\)
\end{mylem}
\begin{proof}
  It might not be obvious, but for all \(n \in \mathbb{N}\)
  \begin{equation}\label{eq:dnfib}
    d_n = -\frac{F_{n+2}}{F_{n+1}},
  \end{equation}
  where \((F_n)_{n=0}^\infty\) is the Fibonacci sequence,
  \[F_{n+2} = F_{n+1} + F_n, \quad F_0 = 0, \quad F_1 = 1.\]
  For \(n = 0\) the \eqref{eq:dnfib} holds since
  \[ -\frac{F_2}{F_1} = -\frac{F_0 + F_1}{F_1} = -\frac{0+1}{1} = -1 = d_0. \]
  Assume \(d_k = \frac{F_{k+2}}{F_{k+1}}\). Then
  \[
    d_{k+1}
      = f^{-1}(d_k) = \frac{1 - d_k}{d_k}
      = \frac{1 + \frac{F_{k+2}}{F_{k+1}}}{-\frac{F_{k+2}}{F_{k+1}}}
      = \frac{F_{k+1} + F_{k+2}}{-F_{k+2}}
      = -\frac{F_{k+3}}{F_{k+2}},
  \]
  which proves using the induction principle proves that \eqref{eq:dnfib} holds
  for all \(n \in \mathbb{N}\).
  From \eqref{eq:dnfib} it directly follows, by lemma \ref{lem:fiblim}, that
  \[\lim_{n \to \infty} d_n = -\frac{\sqrt{5}+1}{2},\]
  which fulfills the proof.
\end{proof}

\begin{mylem}\label{lem:dchain}
  For all \(n \in \mathbb{N}\) the following chain of inequalities hold:
  \begin{equation}\label{eq:dchain}
    d_{2n+1} < d_{2n+3} < -\frac{\sqrt{5}+1}{2} < d_{2n+2} < d_{2n}.
  \end{equation}
\end{mylem}
\begin{proof}
  The lemma can be proved using strong induction.
  The base step is easy to perform since for \(n = 0\) the chain obviously
  holds as
  \[
    d_0 = -1, \quad
    d_1 = -2, \quad
    d_2 = -\frac{3}{2}, \quad
    d_3 = -\frac{5}{3}
  \]
  and obiously
  \[
    -2 < -\frac{5}{3} < -\frac{\sqrt{5}+1}{2} < -\frac{3}{2} < -1.
  \]
  Next the induction step.
  Assume that for some \(p \in \mathbb{N}\) \eqref{eq:dchain} holds for all
  \(q \leq p\), that is
  \begin{equation}\label{eq:dchain-ia}
    \forall q \leq p: \quad
    d_{2q+1} < d_{2q+3} < -\frac{\sqrt{5}+1}{2} < d_{2q+2} < d_{2q}.
  \end{equation}
  To verify the induction step the following separate inequalities need to be
  shown.
  \begin{equation}\label{eq:dchain1}
    d_{2p+3} < d_{2p+5}
  \end{equation}
  \begin{equation}\label{eq:dchain2}
    d_{2p+5} < -\frac{\sqrt{5}+1}{2}
  \end{equation}
  \begin{equation}\label{eq:dchain3}
    -\frac{\sqrt{5}+1}{2} < d_{2p+4}
  \end{equation}
  \begin{equation}\label{eq:dchain4}
    d_{2p+4} < d_{2p+2}
  \end{equation}
  Before continuing the induction assumption \eqref{eq:dchain-ia} immidiately
  gives
  \[ d_1 < \cdots < d_{2p+1} < d_{2p+3} < d_{2p+2} < d_{2p} < \cdots < d_0 \]
  and since \(d_1 = -2\) and \(d_0 = -1\) the following equality holds.
  \begin{equation}\label{eq:dchain-m2m1}
    \quad -2 \leq d_k \leq -1.
  \end{equation}
  for all \(k \leq 2p+3\).

  \eqref{eq:dchain1} and \eqref{eq:dchain4} can be shown simultaneously.
  Let \(k\) be any natural number. It will be useful to express both
  \(d_{k+1}\) and \(d_{k+2}\) in \(d_k\) so the following is computed first.
  \[ d_{k+1} = f^{-1}(d_k) = \frac{1-d_k}{d_k}. \]
  \[
    d_{k+2}
      = f^{-1}(d_{k+1}) = \frac{1-d_{k+1}}{d_{k+1}}
      = \frac{1-\frac{1-d_k}{d_k}}{\frac{1-d_k}{d_k}}
      = \frac{2d_k - 1}{1-d_k}
  \]
  Now the relation between \(d_k\) and \(d_{k+2}\) is to be analysed.
  Let \(\bowtie\) denote either \(<\), \(>\) or \(=\).
  Ofcourse the left and right hand side may not be interchanged, since the
  symbol \(\bowtie\) is symmetric and \(<\) and \(>\) are not.
  \[
    \begin{split}
      d_{k+2} \bowtie d_k
        &\iff d_{k+2} - d_k \bowtie 0 \\
        &\iff \frac{2d_k - 1}{1-d_k} - d_k \bowtie 0 \\
        &\iff \frac{2d_k - 1}{1-d_k} - \frac{d_k-d_k^2}{1-d_k} \bowtie 0 \\
        &\iff \frac{d_k^2 + d_k - 1}{1-d_k} \bowtie 0 \\
        &\iff \frac{(d_k + \frac{1}{2})^2-\frac{5}{4}}{1-d_k} \bowtie 0 \\
        &\iff \frac{(d_k + \frac{1}{2} + \frac{\sqrt{5}}{2})
                    (d_k + \frac{1}{2} - \frac{\sqrt{5}}{2})}{1-d_k} \bowtie 0
    \end{split}
  \]
  A table of signs reveal which relation holds, depending on \(d_k\).
  \begin{table}[H]
    \centering
    \def\arraystretch{1.5}
    \begin{tabular}{c|ccccccc}
      \(d_k\) & &
      \(-\frac{1}{2}-\frac{\sqrt{5}}{2}\) & &
      \(-\frac{1}{2}+\frac{\sqrt{5}}{2}\) & &
      \(1\) & \\
      \hline
      \(d_k + \frac{1}{2} + \frac{\sqrt{5}}{2}\) & \(-\) & \(0\) & \(+\) &
        \(+\) & \(+\)
      & \(+\) & \(+\) \\
      \(d_k + \frac{1}{2} - \frac{\sqrt{5}}{2}\) & \(-\) & \(-\) & \(-\) &
        \(0\) & \(+\)
      & \(+\) & \(+\) \\
      \(1 - d_k\) & \(+\) & \(+\) & \(+\) & \(+\) & \(+\) & \(0\) & \(-\) \\
      \hline
      \(d_{k+2} - d_k\) & \(+\) & \(0\) & \(-\) & \(0\) & \(+\) & undef. & \(-\)
    \end{tabular}
  \end{table}
  \noindent \eqref{eq:dchain1} can be shown by inserting \(k = 2p+3\).
  By assumption \(d_{2p+3} < -\frac{\sqrt{5}+1}{2}\) and by using
  \eqref{eq:dchain-m2m1} it is clear that
  \[ -2 < d_{2p+3} < -\frac{\sqrt{5}+1}{2}. \]
  Using the above table of signs one clearly sees that this implies
  \[ d_{2p+5} - d_{2p+3} > 0 \iff d_{2p+3} < d_{2p+5}, \]
  which shows \eqref{eq:dchain1}.

  \eqref{eq:dchain4} is shown similarly by inserting \(k = 2p+2\). The
  assumption gives \(-\frac{\sqrt{5}+1}{2} < d_{2p+2}\) and by
  \eqref{eq:dchain-m2m1} it is evident that
  \[ -\frac{\sqrt{5}+1}{2} < d_{2p+2} < -1. \]
  The table of signs gives \eqref{eq:dchain4} directly.
  \[ d_{2p+4} - d_{2p+2} < 0 \iff d_{2p+4} < d_{2p+2}. \]

  \eqref{eq:dchain2} and \eqref{eq:dchain3} can also be proven simultaneously.
  Let \(\bowtie\) be as previously and let \(k \in \mathbb{N}\), then
  \[
    \begin{split}
      d_{k+1} \bowtie -\frac{\sqrt{5}+1}{2}
        &\iff d_{k+1} + \frac{\sqrt{5}+1}{2} \bowtie 0 \\
        &\iff \frac{1-d_k}{d_k} + \frac{\sqrt{5}+1}{2} \bowtie 0 \\
        &\iff \frac{2-2d_k}{2d_k} + \frac{d_k\sqrt{5}+d_k}{2d_k} \bowtie 0 \\
        &\iff \frac{2-d_k(1 - \sqrt{5})}{2d_k} \bowtie 0 \\
        &\iff \frac{2(1+\sqrt{5})-d_k(1 - 5)}{2d_k(1+\sqrt{5})} \bowtie 0 \\
        &\iff \frac{4d_k + 2(1+\sqrt{5})}{2d_k(1+\sqrt{5})} \bowtie 0 \\
        &\iff \frac{2d_k +1+\sqrt{5}}{d_k(1+\sqrt{5})} \bowtie 0. \\
    \end{split}
  \]
  As previously a table of signs is useful.
  \begin{table}[H]
    \centering
    \def\arraystretch{1.5}
    \begin{tabular}{c|cccccc}
      \(d_k\) & & \(-\frac{1 + \sqrt{5}}{2}\) & & \(0\) & \\
      \hline
      \(2d_k+1+\sqrt{5}\)              & \(-\) & \(0\) & \(+\) & \(+\)  & \(+\) \\
      \(d_k(1+\sqrt{5})\)              & \(-\) & \(-\) & \(-\) & \(0\)  & \(+\) \\
      \hline
      \(d_{k+1}+\frac{\sqrt{5}+1}{2}\) & \(+\) & \(0\) & \(-\) & undef. & \(+\) \\
    \end{tabular}
  \end{table}
  \noindent \eqref{eq:dchain3} can be shown by inserting \(k=2p+3\).
  By assumption \(d_{2p+3} < -\frac{1+\sqrt{5}}{2}\) and it follows that (look
  at the table)
  \[
    d_{2p+4} + \frac{1+\sqrt{5}}{2} > 0 \iff -\frac{1+\sqrt{5}}{2} < d_{2p+4}.
  \]
  As \eqref{eq:dchain4} has been shown, it is known that \(d_{2p+4} < -1\) and
  using the above result one gets
  \[ -\frac{1+\sqrt{5}}{2} < d_{2p+4} < -1 \]
  but this implies (using \(k=2p+4\) above)
  \[
    d_{2p+5} + \frac{1+\sqrt{5}}{2} < 0 \iff d_{2p+5} < -\frac{1+\sqrt{5}}{2}.
  \]
  This proves \eqref{eq:dchain2} which is the last inequality. Hence, the
  induction step has been successfully proved.

  The strong induction principle now gives the desired result.
\end{proof}

\newcounter{enumTemp}
\noindent Define \(D = \{d_k ; k \in \mathbb{N}\}\),
\(S_n = (d_{2n+1},d_{2n+3})\) and \(T_n = (d_{2n+2},d_{2n})\).
Two consequences of the definition are the following.
\begin{enumerate}
  \item
  \(D \cap S_n = \varnothing\) for all \(n \in \mathbb{N}\).
  \item
  \(D \cap T_n = \varnothing\) for all \(n \in \mathbb{N}\).
  \setcounter{enumTemp}{\theenumi}
\end{enumerate}
Lemma \ref{lem:dchain} shows the following.
\begin{enumerate}
  \setcounter{enumi}{\theenumTemp}
  \item
  \(S_n\) and \(T_n\) exist and are non-empty for all \(n \in \mathbb{N}\).
  \item
  \(S_m \cap T_n = \varnothing\) for all \(m,n \in \mathbb{N}\).
  \item
  \(-\frac{\sqrt{5}+1}{2} \notin D\) for all \(n\).
  \item
  \(-\frac{\sqrt{5}+1}{2} \notin S_n\) for all \(n\).
  \item
  \(-\frac{\sqrt{5}+1}{2} \notin T_n\) for all \(n\).
  \setcounter{enumTemp}{\theenumi}
\end{enumerate}
Lastly lemma \ref{lem:dlim} shows the next very important point.
\begin{enumerate}
  \setcounter{enumi}{\theenumTemp}
  \item
  There exist \(S_n\) and \(T_n\) arbitrarily close to
  \(-\frac{\sqrt{5}+1}{2}\).
\end{enumerate}
This makes it obvious that \(D, S_n, T_n, \{-\frac{\sqrt{5}+1}{2}\}\) are
disjoint for all \(n\) and that their union is \([-2,-1]\). Now if
\(a_k \in [-2,-1]\) exactly one of the below hold.
\begin{enumerate}[(a)]
  \item\label{it:aind}
  \(a_k \in D\).
  \item\label{it:ains}
  \(a_k \in S_n\), for some \(n\).
  \item\label{it:aint}
  \(a_k \in T_n\), for some \(n\).
  \item\label{it:am}
  \(a_k = -\frac{\sqrt{5}+1}{2}\).
\end{enumerate}

It is clear that in the case of \eqref{it:aind} the sequence is undefined. This
is proven strictly.

The sequence will be undefined if it can be shown that for all
\(n \in \mathbb{N}\)
\[a_k = d_n \implies a_j = -1\]
for some \(j\).
For \(n=0\) this is obiously true since \(d_0 = -1\) which shows that \(a_k = -1\).
Assume the statement holds for \(n=p\) that is
\[a_k = d_p \implies a_j = -1\] for
some \(j\).
Let \(a_k = d_{p+1}\). Since
\[a_{k+1} = f(a_k) = f(d_{p+1}) = f(f^{-1}(d_p)) = d_p\]
it is clear that \(a_j = -1\) for some \(j\).
The induction principle now proves the statement.

In the case of \eqref{it:am} the sequence can just be computed as is.
\[
  \begin{split}
    a_{k+1}
      &= f(a_k)
      = \frac{1}{1-\frac{\sqrt{5}+1}{2}}
      = \frac{2}{2-\sqrt{5}-1} \\
      &= \frac{2}{1-\sqrt{5}}
      = \frac{2(1+\sqrt{5})}{1-5}
      = -\frac{1+\sqrt{5}}{2}
  \end{split}
\]
If \eqref{it:am} holds for any \(k\) then it must also hold for \(k+1\). The
induction principle gives that \(a_n = -\frac{\sqrt{5}+1}{2}\) for all
\(n \in \mathbb{N}\).

Only \eqref{it:ains} and \eqref{it:aint} remains.
\begin{mylem}\label{lem:aint0}
  \(\exists k \in \mathbb{N} : a_k \in T_0
    \implies \lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}\)
\end{mylem}
\begin{proof}
  By definition
  \[T_0 = (d_2,d_0) = (-3/2,-1).\]
  \[a_k < -1 \implies \frac{1}{1+a_k} < 0
    \iff a_{k+1} = f(a_k) < 0.\]
  \[a_k > -\frac{3}{2} \implies
    \frac{1}{1 + a_k} < \frac{1}{1-\frac{3}{2}} =
    \frac{1}{-\frac{1}{2}} \iff
    a_{k+1} = f(a_k) < -2\]
  Together these mean \(a_k \in T_0 \implies a_{k+1} \in (-\infty,-2)\), but
  lemma \eqref{thm:aminfm2} now gives the desired result.
\end{proof}

\begin{mythm}
  If there exists a \(k \in \mathbb{N}\) such that \(a_k \in S_m\) or
  \(a_k \in T_m\) for some \(m \in \mathbb{N}\) then
  \[\lim_{n \to \infty} a_n = \frac{\sqrt{5}-1}{2}.\]
\end{mythm}
\begin{proof}
  For \(m = 0\) there are two possibilities, either \(a_k \in S_0\) or
  \(a_k \in T_0\).
  In the latter case lemma \ref{lem:aint0} gives the result.
  Assume \(a_k \in S_0\).
  Lemma \ref{lem:ab2} gives the following implication
  \[a_k \in S_0 = (d_1,d_3)
    \implies a_{k+1} = f(a_k) \in (f(d_3),f(d_1)) = (d_2,d_0) = T_0\]
  and the result follows from the case when \(a_k \in T_0\). Thus the theorem is
  true for \(m = 0\).

  Assume the theorem holds for \(m = p\).
  First consider the case when \(a_k \in T_{p+1}\).
  Since \(a_k \in T_{p+1} = (d_{2p+4},d_{2p+2})\) lemma \ref{lem:ab2} gives
  \[a_{k+1} = f(a_k) \in (f(d_{2p+2}),f(d_{2p+4})) = (d_{2p+1},d_{2p+3}) = S_p\]
  and the result follows from the induction hypothesis.
  Next consider the case when \(a_k \in S_{p+1}\).
  \(a_k \in S_{p+1} = (d_{2p+3},d_{2p+5})\) gives directly, again using lemma
  \ref{lem:ab2},
  \[a_{k+1} = f(a_k) \in
    (f(d_{2p+5}),f(d_{2p+3})) = (d_{2p+4},d_{2p+2}) = T_{p+1}\]
  and the result follows from the case when \(a_k \in T_{p+1}\).

  The above and the induction principle prove that the theorem is true for all
  \(m \in \mathbb{N}\).
\end{proof}

\noindent Now all cases of \(a_0 \in \mathbb{R}\) have been covered and a
conclusion is in place.

\subsection*{Conclusion}
It has been shown that the sequence
\[a_{n+1} = f(a_n), \quad f(x) = \frac{1}{1+x}\]
converges to \(\frac{\sqrt{5}-1}{2}\) regardless of the choice
\(a_0 \in \mathbb{R}\), except if \(a_0 = -\frac{\sqrt{5}+1}{2}\) in which
case the sequence is constant, or if \(a_0\) makes any \(a_n\) undefined
through a division by zero.
Thus the chain fraction
\[\cfrac{1}{1+\cfrac{1}{1+\cfrac{1}{1+\cdots}}}\]
can have two values, the obvious one being \(\frac{\sqrt{5}-1}{2}\) and the less
obvious being \(-\frac{\sqrt{5}+1}{2}\), assuming the fraction is defined at all
(no division by zero).

The naive answer to if the chain fraction makes sence is yes since for the vast
majority of \(a_0\) it converges to a \(\frac{\sqrt{5}-1}{2}\), but a
pessimistic interpretation makes the fraction completely different, if defined
at all.
Usage of the chain fraction might cause confusion.

\end{document}
