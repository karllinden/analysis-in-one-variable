#!/usr/bin/env python3

print("I will in a terribly inefficient manner compute arctan(x) with "
      "arbitrary precision for you.")

x = input("x? ")
x = float(x)
prec = input("Precision? ")
prec = float(prec)

# E_N = (U_N - L_N)/2 = x^3/(2N*(1+x^2) < prec
#                    <=> N > x^3/(2*prec*(1+x^2))
N = int((1/(prec*2)) * x**3/(1+x**2) / 2) + 1

print("N =", N)

x_square = x**2
N_square = N**2

# Returns the k:th term.
def term(k):
    global N_square, x_square
    return 1/(N_square + k**2 * x_square)

t0 = term(0)
tN = term(N)

# The sum of t_1, ..., t_{N-1}.
s = 0
for k in range(1, N):
    s += term(k)

# (U+L)/2 = (x*N*(t0 + s) + x*N*(s + tN))/2 = x*N((t0 + tN)/2 + s)
# E = (U-L)/2 = x*N*((t0 + s - s - tN)/2) = x*N*(t0 - tN)/2
print("arctan(x) ~=", x*N*(s + (t0 + tN)/2))
print("error ~=", x*N*(t0 - tN)/2)
