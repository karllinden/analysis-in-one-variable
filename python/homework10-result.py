#!/usr/bin/env python3

x = 6
n = 8

x_square = x**2

s = 1
for m in range(n, 0, -1):
    s = 1 - x_square / (2*m*(2*m+1)) * s
s *= x

print(s)
