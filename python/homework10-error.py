#!/usr/bin/env python3

import fractions


# f(0) = 6^3/3! = 6^2 = 36
a = fractions.Fraction(36, 1)

# treshold
tres = fractions.Fraction(1, 100)

m = 4 # m = 2n + 4
n = 0
while a >= tres:
    # f(n) = 6^(2n+3)/(2n+3)!
    # f(n+1) = 6^(2n+5)/(2n+5)! = f(n) * 6^2/((2n+4)(2n+5))
    a *= 36
    a /= m # m = 2n+4
    m += 1
    a /= m # m = 2n+5
    m += 1 # m = 2n + 6 = 2(n+1) + 4
    n += 1

    print("n =",n, ": a =", a, ": a ~=", float(a))
