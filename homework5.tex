\documentclass[a4paper]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newcommand{\dx}{\mathrm{d}x}
\newcommand{\ddx}{\frac{\mathrm{d}}{\dx}}

\begin{document}

\title{Exercise 3.9 and 3.17\\ 5th homework for Analysis in one variable}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle
\vfill
\input{title-footer.tex}
\clearpage

\section*{Exercise 3.9}

Since the solution of \textbf{(a)}, \textbf{(b)} and \textbf{(c)} do not differ
in method they can be solved in parallel.

Let
\[
  \begin{split}
    g_1(x) &= \ln \left(\frac{1}{x}\right) + \ln{x} = f(f'(x)) + f(x), \\
    g_2(x) &= \ln(x^a) - a\ln{x} = f(x^a) - af(x), \\
    g_3(x) &= \ln(ax) - \ln{a} - \ln{x} = f(ax) - f(a) - f(x).
  \end{split}
\]
Differentiation yields
\[
  \begin{split}
    g_1'(x) &= f'(f'(x)) \cdot \ddx f'(x) + f'(x), \\
    g_2'(x) &= f'(x^a) \cdot ax^{a-1} - af'(x), \\
    g_3'(x) &= af'(ax) - f'(x).
  \end{split}
\]
Insertion of \(f'(x) = \frac{1}{x}\) gives
\[
  \begin{split}
    g_1'(x)
      &= \frac{1}{\frac{1}{x}} \cdot \ddx \frac{1}{x} + \frac{1}{x}
       = x \cdot \left( -\frac{1}{x^2} \right) + \frac{1}{x}
       = 0, \\
    g_2'(x)
      &= \frac{1}{x^a} \cdot ax^{a-1} - \frac{a}{x}
       = \frac{a}{x} - \frac{a}{x}
       = 0, \\
    g_3'(x)
      &= \frac{a}{ax} - \frac{1}{x} = 0,
  \end{split}
\]
but this means all \(g_i(x)\) are constant. Computation of \(g(1)\) yields
\[
  \begin{split}
    g_1(1) &= f(f'(1)) + f(1) = f(1) + 0 = 0, \\
    g_2(1) &= f(1^a) - af(1) = f(1) - a\cdot0 = 0, \\
    g_3(1) &= f(a\cdot1) - f(a) - f(1) = f(a) - f(a) - 0 = 0,
  \end{split}
\]
which means \(g_i(x) = 0\) for all \(x\) and it can be deduced that
\[
  \begin{split}
    \ln \left(\frac{1}{x}\right) + \ln{x} = 0
    &\iff
    \ln \left(\frac{1}{x}\right) = -\ln{x}, \\
    \ln(x^a) - a\ln{x} = 0 &\iff \ln{x^a} = a\ln{x}, \\
    \ln(ax) - \ln{a} - \ln{x} = 0 &\iff \ln(ax) = \ln{a} - \ln{x}.
  \end{split}
\]

\clearpage

\section*{Exercise 3.17}

\[ f(x) = \frac{x^2+1}{|x|+1} \]
is defined on all of \(\mathbb{R}\) since the denominator is always positive
as \(|x| \geq 0\) for all \(x\).
Also the function is continuous, since it is a quotient of continuous functions.
To ease differentiation the function can be split up in two cases.
\[
  f(x) =
  \begin{cases}
    f_1(x) &\text{if} \quad x \geq 0 \\
    f_2(x) &\text{if}\quad x < 0
  \end{cases}
\]
where
\[
  f_1(x) = \frac{x^2+1}{1+x}
  \quad \text{and} \quad
  f_2(x) = \frac{x^2+1}{1-x}.
\]
Polynomial division yields
\begin{equation}\label{eq:star}
  \tag{*}
  f_1(x) = x - 1 + \frac{2}{1+x}
  \quad \text{and} \quad
  f_2(x) = -x - 1 + \frac{2}{1-x}
\end{equation}
Differentiation gives
\[
  f_1'(x) = 1 - \frac{2}{(1+x)^2}, \; x > 0,
  \quad
  f_2'(x) = -1 + \frac{2}{(1-x)^2}, \; x < 0.
\]
Note that neither \(f_1\) nor \(f_1'\) is defined for \(x=-1\), but that does
not matter since \(x=-1\) is not in the domain of any of them.
A similar note holds for \(f_2\) and \(f_2'\), but with \(x=1\).
Anyway, the derivatives are defined on all other values of \(x\).

To show that \(f\) is not differentiable at \(x=0\) it is sufficient to compute
\(f_1\) and \(f_2\) at \(x=0\) and realise that they differ.\footnote{
A more correct approach would be to take the limits of both derivatives as \(x\)
goes to zero, since they are not formally defined at zero, but the extensions of
both derivatives are continuous and defined in a neighbourhood of \(x=0\) so
insertion suffices.}
This yields
\[ f_1'(0) = -1 \quad \text{and} \quad f_2'(0) = 1. \]
However, on all other \(x\) the derivative \(f'\) is defined.
The zeroes of the derivatives are
\[
  f_1'(x) = 0 \iff x = -1 \pm \sqrt{2}
  \quad \text{and} \quad
  f_2'(x) = 0 \iff x = 1 \pm \sqrt{2}.
\]
Obviously \(x=\sqrt{2}-1\) is the only relevant zero to \(f_1'\) and
\(x=1-\sqrt{2}\) to \(f_2'\).

Only \(f''\) is yet to be studied before a table of signs can be constructed.
\[
  f_1''(x) = \frac{4}{(1+x)^3}, \; x > 0,
  \quad
  f_2''(x) = \frac{4}{(1-x)^3}, \; x < 0.
\]
It is evident that \(f_1''(x) > 0\) whenever \(x > 0\) and that
\(f_2''(x) > 0\) when \(x < 0\). It can be concluded that \(f\) is convex on
all of \(\mathbb{R}\), if the detail that \(f''\) is not defined in \(x=0\) is
disregarded.
Finally, a table of signs can be constructed.
\begin{table}[H]
  \centering
  \begin{tabular}{c|ccccccc}
    \(x\)        &              & \(1-\sqrt{2}\)  &             & 0 &              & \(\sqrt{2}-1\)  & \\
    \hline
    \(f_1''(x)\) & !            & !               & !           & ! & \(+\)        & \(+\)           & \(+\)       \\
    \(f_2''(x)\) & \(+\)        & \(+\)           & \(+\)       & ! & !            & !               & !           \\
    \(f_1'(x)\)  & !            & !               & !           & ! & \(-\)        & \(0\)           & \(+\)       \\
    \(f_1'(x)\)  & \(-\)        & \(0\)           & \(+\)       & ! & !            & !               & !           \\
    \hline
    \(f''(x)\)   & \(+\)        & \(+\)           & \(+\)       & ! & \(+\)        & \(+\)           & \(+\)       \\
    \(f'(x)\)    & \(-\)        & \(0\)           & \(+\)       & ! & \(-\)        & \(0\)           & \(+\)       \\
    \(f(x)\)     & \(\searrow\) & \(2\sqrt{2}-2\) & \(\nearrow\)& 1 & \(\searrow\) & \(2\sqrt{2}-2\) & \(\nearrow\)
  \end{tabular}
\end{table}

From the table it is seen that \(f\) has two global minima at \(x=1-\sqrt{2}\)
and \(x=\sqrt{2}-1\).
Also \(f\) has a local maximum at \(x = 0\), which is not a global maximum
since, for example, \(f(2) = \frac{5}{3} > 1\).
At the extremal points \(f(x)\) has been written out, since the computation is
trivial.

Last are the asymptotes, but from \eqref{eq:star} it is seen that \(y = x-1\)
and \(y = -x-1\) are oblique asymptotes as \(x \to \infty\) and
\(x \to -\infty\), respectively.

Only a sketch of the function remains.
Below is a plot with the asymptotes and ``tangents to \(x=0\)'' indicated.
Also the lower bound has been drawn.

\begin{figure}[H]
  \centering
  \input{plots/317.tex}
\end{figure}

\end{document}
