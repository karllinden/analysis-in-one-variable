\documentclass[a4paper]{article}

\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newtheorem{mylem}{Lemma}
\newtheorem{mypro}{Proposition}
\newtheorem{mythm}{Theorem}

\begin{document}

\title{The secant method}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle
\vfill
\input{title-footer.tex}
\clearpage

\section*{Background}

In numeric approximation it is often required to find the zeroes of a continuous
real valued function, when an interval where the function has different signs in
the endpoints is known.
A simple algorithm that can do this is the secant method in which the interval
is reduced by computing the intersection between the x-axis and the secant
between the endpoints.
It is desirable to determine under which conditions the secant method works, and
how fast it converges.

\section*{Task}

\begin{enumerate}
  \item
  Formulate an algorithm for the secant method,
  \item
  prove that it converges, and,
  \item
  determine the order of convergence.
\end{enumerate}

\section*{Solution}

Let the function be \(f\) and the initial interval be \([a_0,b_0]\). It can be
assumed that
\[ f(a_0) < 0 \quad \text{and} \quad f(b_0) > 0 \]
since if at least one of \(a_0\) or \(b_0\) is zero a zero has already been
found and there is no need to continue and if \(f(a_0) > 0\) and \(f(b_0) < 0\)
the situation can be returned to the above assumption by considering \(-f\)
instead.
Lastly the situation where \(a_0\) and \(b_0\) have the same sign can be
dismissed since \(f\) is, in that case, not guaranteed to have a zero in
\([a_0,b_0]\).

The idea is to create successively smaller intervals \([a_n,b_n]\) where the
above condition hold for \(n\) as well.
This can be done by letting \(c_n\) denote the intersection between the secant
from \(a_n\) to \(b_n\).
Two expressions for the secant line are
\[
  y - f(a_n) = \frac{f(b_n) - f(a_n)}{b_n - a_n} (x - a_n)
\]
and
\[
  y - f(b_n) = \frac{f(b_n) - f(a_n)}{b_n - a_n} (x - b_n).
\]
By inserting \((x,y) = (c_n,0)\) and solving for \(c_n\) one gets
\[
  c_n
    = a_n - f(a_n) \frac{b_n - a_n}{f(b_n)-f(a_n)}
    = b_n - f(b_n) \frac{b_n - a_n}{f(b_n)-f(a_n)}.
\]
If \(f(c_n) = 0\) the zero is found and there is nothing more to do.
If \(f(c_n) > 0\), then \([a_{n+1},b_{n+1}] = [a_n,c_n]\)
should be a smaller interval fulfilling the above condition.
Otherwise \([a_{n+1},b_{n+1}] = [c_n,b_{n+1}]\) should be a candidate.
As this process is repeated it should converge to a zero and with the maximum
error being the difference \(b_n - a_n\).
With the above discussion in mind it is possible to formulate the algorithm.

\begin{algorithm}[H]
\caption{The secant method}
\label{alg:raw}
\begin{algorithmic}[5]
  \Function{secantMethod}{\(g\),\(a\),\(b\),maxError}
    \If{\(a > b\)}
      \State Swap \(a\) and \(b\).
    \EndIf

    \State \(\alpha \gets g(a)\)
    \State \(\beta \gets g(b)\)

    \If{\(\alpha = 0\)}
      \State \Return \(a\)
    \ElsIf{\(\beta = 0\)}
      \State \Return \(b\)
    \ElsIf{\(\alpha < 0\) and \(\beta > 0\)}
      \State Let \(f = g\)
    \ElsIf{\(\alpha > 0\) and \(\beta < 0\)}
      \State Let \(f = -g\)
      \State \(\alpha \gets -\alpha\)
      \State \(\beta \gets -\beta\)
    \Else
      \State error
    \EndIf

    \While{\(b - a < \text{maxError}/2\)}
      \State \(c \gets a - \alpha \frac{b - a}{\beta - \alpha}\)
      \State \(\gamma \gets f(c)\)
      \If{\(\gamma < 0\)}
        \State \(a \gets c\)
        \State \(\alpha \gets \gamma\)
      \ElsIf{\(\gamma > 0\)}
        \State \(b \gets c\)
        \State \(\beta \gets \gamma\)
      \Else
        \State \Return \(c\)
      \EndIf
    \EndWhile
    \State \Return \(\frac{a+b}{2}\)
  \EndFunction
\end{algorithmic}
\end{algorithm}

\noindent
The \textsc{secantMethod} function takes the argument \(g\) which is the initial
function.
\(g\) is then reflected if appropriate to produce \(f\), which will be used in
the iteration.
Note that the algorithm is optimized to reduce the number of calls to \(f\) by
storing the function values in \(\alpha\), \(\beta\) and \(\gamma\), as \(f\)
might be a slow function.
Also note that no indices are used for \(a\) and \(b\), but let from now on
\[
  a_n =
    \text{the value of \(a\) after \(n\) iterations of the \textbf{while} loop}
\]
and let \(b_n\) be defined similarly.

First the following obvious proposition is needed for the further discussion.
\begin{mypro}\label{pro:fineq}
  \(f(a_n) < 0\) and \(f(b_n) > 0\) for all \(n\).
\end{mypro}
\begin{proof}
  Before entering the \textbf{while} loop the proposition is by construction
  fulfilled for \(n=0\).
  Assume the proposition is true after \(n = p\) iterations.
  Then for the \(n = p+1\):th iteration it holds that if \(f(c_p) < 0\) then
  \[ a_{p+1} = c_p \quad \text{and} \quad b_{p+1} = b_p. \]
  This makes the proposition true for \(n = p+1\) since
  \[ f(a_{p+1}) = f(c_p) < 0 \quad \text{and} \quad f(b_{p+1}) = f(b_p) > 0. \]
  On the other hand, if \(f(c_p) > 0\) then
  \[ a_{p+1} = a_p \quad \text{and} \quad b_{p+1} = c_p. \]
  and the proposition is true for \(n = p + 1\) since
  \[ f(a_{p+1}) = f(a_p) < 0 \quad \text{and} \quad f(b_{p+1}) = f(c_p) > 0. \]
  Lastly, if \(f(c_p) = 0\) the algorithm stops and there exist no \(p+1\):th
  iteration.
  By the induction principle the proposition is true for all \(n\).
\end{proof}

\begin{mypro}\label{pro:ineq}
  \(a_{n-1} \le a_n < b_n \le b_{n-1}\) for all \(n \ge 1\).
\end{mypro}
\begin{proof}
  The inequality
  \[ a_0 < b_0 \]
  must be true, since if \(a_0 > b_0\) then they have been swapped so that
  \(a_0 < b_0\) and if \(a_0 = b_0\) then \(f(a_0) = f(b_0)\).
  In this case, if \(f(a_0) = f(b_0) = 0\) then the algorithm has stopped in
  the second \textbf{if} clause.
  Otherwise \(f(a_0) = f(b_0) \ne 0\), so \(f(a_0)\) and \(f(b_0)\) have the
  same sign and the algorithm has stopped due to error (in the \textbf{else}
  clause).

  First the following implication is shown.
  \begin{equation}\label{eq:proineq}
    a_p < b_p \implies a_p \le a_{p+1} < b_{p+1} \le b_p
  \end{equation}
  First note
  \[ \frac{b_p - a_p}{f(b_p) - f(a_p)} > 0 \]
  since both numerator and denominator are positive by assumption and
  proposition \ref{pro:fineq}.
  Assume \(a_p < b_p\), then
  \[ c_p = a_p - f(a_p) \frac{b_p - a_p}{f(b_p) - f(a_p)}, \]
  but proposition \ref{pro:fineq} gives \(f(a_p) < 0\) so \(c_p > a_p\).
  Also
  \[ c_p = b_p - f(b_p) \frac{b_p - a_p}{f(b_p) - f(a_p)}, \]
  and again proposition \ref{pro:fineq} gives \(f(b_p) > 0\) so \(c_p < b_p\).
  Hence, \(a_p < c_p < b_p\).
  If \(f(c_p) = 0\) the algorithm stops and in that case there exist no iteration
  \(p+1\).
  Otherwise, depending on the sign of \(f(c_p)\), either
  \[
    \begin{cases}
      a_{p+1} = c_p \\
      b_{p+1} = b_p
    \end{cases}
    \quad
    \text{or}
    \quad
    \begin{cases}
      a_{p+1} = a_p \\
      b_{p+1} = c_p
    \end{cases}.
  \]
  In both cases
  \[ a_p \le a_{p+1} < b_{p+1} \le b_p \]
  and the implication \eqref{eq:proineq} is shown.

  For \(n = 1\) the proposition is true by insertion of \(p = 0\) in
  \eqref{eq:proineq} and the initial section of this proof.
  Assume the inequality is true for \(n - 1\).
  Insertion of \(p = n - 1\) in \eqref{eq:proineq} directly yields the
  desired result.
  This and the induction principle completes the proof.
\end{proof}

\noindent
It is now possible to show that the sequences \((a_n)_{n=0}^\infty\) and
\((b_n)_{n=0}^\infty\) converge to the zero when they are infinite.
This proposition is the one that guarantees that the algorithm always works.

\begin{mypro}
  If \(f(c_n) \ne 0\) for all \(n\), then for some \(C \in [a_0,b_0]\)
  \[
    \lim_{n \to \infty} a_n = \lim_{n \to \infty} b_n = C
  \]
  and
  \[
    f(C) = 0.
  \]
\end{mypro}
\begin{proof}
  If \(f(c_n) \ne 0\) for all \(n\), then \(n\) can grow sufficiently large,
  making \(n \to \infty\) meaningful.
  By proposition \ref{pro:ineq} it is known that
  \[ a_0 \le a_1 \le a_2 \le \cdots < \cdots \le b_2 \le b_1 \le b_0. \]
  From this it is seen that \((a_n)_{n=0}^\infty\) is a increasing sequence with
  an upper bound, such as for example \(b_0\), and that \((b_n)_{n=0}^\infty\)
  decreasing sequence with a lower bound, such as \(a_0\).
  Thus the sequences converge.
  It remains to be shown that they converge to the same number.

  Let
  \[
    \lim_{n \to \infty} a_n = A
    \quad \text{and} \quad
    \lim_{n \to \infty} b_n = B
  \]
  and assume \(A \ne B\), then
  \[ a_0 \le a_1 \le \cdots \le A < B \le \cdots \le b_1 \le b_0. \]
  Otherwise, if for example \(a_k > A\), then it would be possible to choose an
  \(\epsilon > 0\) such that \(a_k = A + \epsilon\), but this contradicts that
  \(a_n \to A\) as \(n \to \infty\) converges since
  \(a_l \ge a_k = A + \epsilon\) for all \(l > k\).
  A similar result, holds for \((b_n)_{n=0}^\infty\) and \(B\).

  Next it shall be shown that atleast one of
  \[ a_n < A \quad \text{and} \quad b_n > B \]
  must hold for all \(n\).
  This is proven using proof by contradiction.
  Thus, assume
  \begin{equation}\label{eq:abeq}
    a_k = A \quad \text{and} \quad b_k = B
  \end{equation}
  for some \(k = n\).
  Assume \eqref{eq:abeq} holds for some \(k = p\).
  The following inequalities hold.
  \[ a_p \le a_{p+1} \le A \quad \text{and} \quad b_p \ge b_{p+1} \ge B. \]
  But since \(a_p = A\) and \(b_p = B\) it must hold that
  \[ a_p = a_{p+1} = A \quad \text{and} \quad b_p = b_{p+1} = B. \]
  Since it was assumed \eqref{eq:abeq} holds for \(k = n\), then it is by
  induction clear that \eqref{eq:abeq} holds for all \(k \ge n\).
  For any \(k \ge n\),
  \[
    c_k
      = a_k - f(a_k) \frac{b_k - a_k}{f(b_k) - f(a_k)}
      = b_k - f(b_k) \frac{b_k - a_k}{f(b_k) - f(a_k)}.
  \]
  But \(a_{k+1} = c_k\) or \(b_{k+1} = c_k\) so
  \[
    a_{k+1} = a_k - f(a_k) \frac{b_k - a_k}{f(b_k) - f(a_k)}
    \quad \text{or} \quad
    b_{k+1} = b_k - f(b_k) \frac{b_k - a_k}{f(b_k) - f(a_k)}.
  \]
  Using the fact that \(a_k = a_{k+1} = A\) and \(b_{k+1} = b_k = B\) this is
  the same as
  \[
    f(a_k) \frac{b_k - a_k}{f(b_k) - f(a_k)} = 0
    \quad \text{or} \quad
    f(b_k) \frac{b_k - a_k}{f(b_k) - f(a_k)} = 0,
  \]
  but \(b_k - a_k\) is non-zero and \(f(b_k) - f(a_k)\) too, so it can be
  deduced that
  \[ f(a_k) = 0 \quad \text{or} \quad f(b_k) = 0, \]
  which contradicts proposition \ref{pro:fineq} and the assumption that the
  sequences are infinite.
  Hence, the assumption that \eqref{eq:abeq} holds for some \(n\) must be false
  and it can be concluded that at least one of
  \[ a_n < A \quad \text{and} \quad b_n > B \]
  must hold for all \(n\).

  Assume \(a_n < A\) for all \(n\) the case when \(b_n > B\) is handled
  similarly.
  By the definition of limit there must for all \(\epsilon > 0\) exist an
  \(a_k\) such that
  \[ A - \epsilon < a_k < A. \]
  As \(a_k = A - \epsilon'\) for some \(\epsilon' > 0\), the definition of limit
  again gives that there must exist an \(a_l\) such that
  \[ A - \epsilon' < a_l < A. \]
  Let \(l\) be the least integer such that the inequality holds.
  Hence,
  \[
    A - \epsilon < a_k < a_l < A
    \quad \text{and} \quad
    a_k = a_{k+1} = \cdots = a_{l-1} < a_l.
  \]
  FIXME: Complete.
\end{proof}

\noindent
\textbf{Note.}
What has been done above can be seen as a statement and proof of the
intermediate value theorem.
Hence, the intermediate value theorem can be seen as a consequence of the fact
that the algorithm always works.

\end{document}
