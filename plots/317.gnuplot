set terminal epslatex

# The out variable is given by the Makefile. This avoids hardcoding filenames.
set output out

set xlabel "$x$"
set ylabel "$y$"
set samples 100000
set nokey
set monochrome
set palette gray
plot [x=-5:5] [y=0:4.5] \
     (x**2+1)/(abs(x)+1), \
     x-1 w lines lt 2, \
     -x-1 w lines lt 2, \
     x+1 w lines lt 3, \
     -x+1 w lines lt 3, \
     2*sqrt(2)-2 w lines lt 4
