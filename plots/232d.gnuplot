set terminal epslatex

# The out variable is given by the Makefile. This avoids hardcoding filenames.
set output out

set xlabel "$x$"
set ylabel "$y$"
set key top left
set monochrome
set palette gray
plot [x=0:10] \
     x*log(x)-x   title "$x\ln{x}-x$", \
     x*log(x)-x+1 title "$x\ln{x}-x+1$", \
     x*log(x)-x+2 title "$x\ln{x}-x+2$"
