set terminal epslatex

# The out variable is given by the Makefile. This avoids hardcoding filenames.
set output out

set xlabel "$x$"
set ylabel "$y$"
set samples 10000
set key spacing 5 top right
set monochrome
set palette gray
plot [x=1:10] [y=0:1.2] \
     1./floor(x)**2 title "\\(\\frac{1}{\\lfloor x \\rfloor^2}\\)", \
     1./(x-1)**2    title "\\(\\frac{1}{(x-1)^2}\\)"
