\documentclass[a4paper,12pt]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}
\usepackage{pst-plot}
\usepackage{pstricks}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\renewcommand{\d}[1]{\mathrm{d}{#1}}
\newcommand{\dd}[1]{\frac{\mathrm{d}}{\d{#1}}}

\everymath{\displaystyle}

\begin{document}

\title{Homework 7 \\ Analysis in one variable}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle
\vfill
\input{title-footer.tex}
\clearpage

\begin{enumerate}[label=\textbf{(\alph*)}]
  \item
  \begin{enumerate}[label=\textbf{(\roman*)}]
    \item
    The following computation makes it evident that \(f\) is an odd function,
    since for all \(x\) it holds that
    \[
      \begin{split}
        f(-x)
          &=
            \arctan(-x) - 2 \int_0^{-x} \frac{1}{(1+t^2)^2} \d{t}
          \\
          &=
            \left[
              \begin{matrix}
                t  = -s & \d{t} = (-1)\d{s} \\
                0 = -0  & -x = -(x)
              \end{matrix}
            \right]
          \\
          &=
            -\arctan{x} - 2 \int_0^x \frac{1}{(1+(-s)^2)^2} (-1) \d{s}
          \\
          &=
            -\arctan{x} + 2 \int_0^x \frac{1}{(1+s^2)^2} \d{s}
          \\
          &=
            -f(x)
      \end{split}
    \]

    \item
    \(f(x)\) is defined for all \(x \in \mathbb{R}\).
    Because \(f\) is differentiable, by the fundamental theorem of calculus,
    it is also continuous.
    Differentiation yields
    \begin{equation}\label{eq:der}
      \begin{split}
        f'(x)
          &=
            \frac{1}{1+x^2} - 2 \dd{x} \int_0^x \frac{1}{(1+t^2)^2} \d{t}
          \\
          &\overset{\text{FTC}}{=}
            \frac{1}{1+x^2} - \frac{2}{(1+x^2)^2}
          \\
          &=
            \frac{1+x^2-2}{(1+x^2)^2}
          =
            \frac{x^2-1}{(1+x^2)^2}
          =
            \frac{(x+1)(x-1)}{(1+x^2)^2}.
      \end{split}
    \end{equation}
    Thus, stationary points of \(f\) are \(x=\pm 1\).
    From this information a table of signs that reveal the answer can be
    constructed.
    \begin{table}[H]
      \centering
      \renewcommand{\arraystretch}{1.2}
      \begin{tabular}{c|ccccc}
        \(x\)     &              & \(-1\)    &              & \(1\)     &              \\
        \hline
        \(x+1\)   & \(-\)        & \(0\)     & \(+\)        & \(+\)     & \(+\)        \\
        \(x-1\)   & \(-\)        & \(-\)     & \(-\)        & \(0\)     & \(+\)        \\
        \hline
        \(f'(x)\) & \(+\)        & \(0\)     & \(-\)        & \(0\)     & \(+\)        \\
        \(f(x)\)  & \(\nearrow\) & loc. max. & \(\searrow\) & loc. min. & \(\nearrow\) \\
      \end{tabular}
    \end{table}

    \item
    The second derivative of \(f\) is
    \[
      \begin{split}
        f''(x)
          &=
            \frac{2x(1+x^2)^2 - (x^2-1)2(1+x^2)2x}{(1+x^2)^4}
          \\
          &=
            \frac{2x\left((1+x^2) - 2(x^2-1)\right)}{(1+x^2)^3}
          \\
          &=
            \frac{2x(3-x^2)}{(1+x^2)^3}
          =
            \frac{2x(\sqrt{3}-x)(\sqrt{3}+x)}{(1+x^2)^3}.
      \end{split}
    \]
    A table of signs reveal where \(f\) is convex (\(\cup\)) and concave
    (\(\cap\)) respectively.
    \begin{table}[H]
      \centering
      \renewcommand{\arraystretch}{1.2}
      \begin{tabular}{c|ccccccc}
        \(x\)          &          & \(-\sqrt{3}\) &          & \(0\) &          & \(\sqrt{3}\) &          \\
        \hline
        \(\sqrt{3}+x\) & \(-\)    & \(0\)         & \(+\)    & \(+\) & \(+\)    & \(+\)        & \(+\)    \\
        \(2x\)         & \(-\)    & \(-\)         & \(-\)    & \(0\) & \(+\)    & \(+\)        & \(+\)    \\
        \(\sqrt{3}-x\) & \(+\)    & \(+\)         & \(+\)    & \(+\) & \(+\)    & \(0\)        & \(-\)    \\
        \hline
        \(f''(x)\)     & \(+\)    & \(0\)         & \(-\)    & \(0\) & \(+\)    & \(0\)        & \(-\)    \\
        \(f(x)\)       & \(\cup\) &               & \(\cap\) &       & \(\cup\) &              & \(\cap\)
      \end{tabular}
    \end{table}

    \item
    It is possible to sketch the following plot by hand, but it is hardly
    worthwhile.
    \begin{center}
      \psset{unit=1.8cm}
      \begin{pspicture*}(-3,-.7)(3.2,.9)
        \psline[linewidth=1pt]{->}(0,-.7)(0,.7) % y axis
        \psline[linewidth=1pt]{->}(-3,0)(3,0) % x axis
        \psline[linewidth=.5pt]{-}(-1.732,-.07)(-1.732,.07)
        \psline[linewidth=.5pt]{-}(-1,-.07)(-1,.07)
        \psline[linewidth=.5pt]{-}(1,-.07)(1,.07)
        \psline[linewidth=.5pt]{-}(1.732,-.07)(1.732,.07)
        \psline[linewidth=.5pt]{-}(-.07,.5)(.07,.5)
        \psline[linewidth=.5pt]{-}(-.07,-.5)(.07,-.5)
        \rput(3.1,0){$x$}
        \rput(0,.8){$y$}
        \rput(-1.732,-.2){$-\sqrt{3}$}
        \rput(-1,-.2){$-1$}
        \rput(1,-.2){$1$}
        \rput(1.732,-.2){$\sqrt{3}$}
        \rput[l](.1,.5){$1/2$}
        \psline[linewidth=.5pt](2.35,.8)(2.65,.8)
        \rput[l](2.7,.8){$f(x)$}
        \psplot[plotstyle=curve,linewidth=.5pt]{-3}{3}
          {x 1 x x mul add div neg}
      \end{pspicture*}
    \end{center}
  \end{enumerate}

  \item
  The function
  \[ g(x) = \frac{x}{x^2+1} \]
  has the derivative
  \[ g'(x) = \frac{1(x^2+1)-x\cdot2x}{(x^2+1)^2} = \frac{1-x^2}{(x^2+1)^2}. \]
  By using \eqref{eq:der} this means
  \[ g'(x) = -f'(x) \iff g'(x) + f'(x) = 0 \iff g(x) + f(x) = C, \]
  for some constant \(C\).
  Insertion of \(x = 0\) yields
  \[
    C = g(0) + f(0)
      = \frac{0}{0^2+1} + \arctan{0} - 2\int_0^0 \frac{1}{(1+t^2)^2} \d{t}
      = 0.
  \]
  Hence,
  \[
    g(x) = -f(x)
    \iff
    \frac{x}{x^2+1} = -\arctan{x} + 2\int_0^x \frac{1}{(1+t^2)^2} \d{t}
  \]
  and it can be deduced that
  \[
    \int_0^x \frac{1}{(1+t^2)^2} \d{t}
      = \frac{1}{2} \left(\frac{x}{x^2+1} + \arctan{x}\right).
  \]
\end{enumerate}

\noindent \textbf{Extra:}
First the indefinite integral is computed, by using the following trick.
\[
  \begin{split}
    \arctan{x}
      &=
        \int 1 \cdot \frac{1}{1+x^2} \d{x}
      \\
      &=
        x \cdot \frac{1}{1+x^2} -
        \int x \cdot \left(-\frac{2x}{(1+x^2)^2}\right) \d{x}
      \\
      &=
        \frac{x}{1+x^2} + \int \frac{2x^2}{(1+x^2)^2} \d{x}
      \\
      &=
        \frac{x}{1+x^2} + 2\int \frac{1 + x^2 - 1}{(1+x^2)^2} \d{x}
      \\
      &=
        \frac{x}{1+x^2} +
        2\int \left(\frac{1}{1+x^2} - \frac{1}{(1+x^2)^2}\right) \d{x}
      \\
      &=
        \frac{x}{1+x^2} +
        2 \int \frac{1}{1+x^2} \d{x} - 2 \int \frac{1}{(1+x^2)^2} \d{x}
      \\
      &=
        \frac{x}{1+x^2} + 2\arctan{x} - 2 \int \frac{1}{(1+x^2)^2} \d{x}
  \end{split}
\]
By moving terms around and dividing by two on both sides, one can deduce
\[
  \int \frac{1}{(1+x^2)^2} \d{x}
    = \frac{1}{2} \left(\frac{x}{1+x^2} + \arctan{x}\right).
\]
As the right hand side is zero when \(x\) is zero, it should be clear that
\[
  \int_0^x \frac{1}{(1+t^2)^2} \d{t}
    = \frac{1}{2} \left(\frac{x}{1+x^2} + \arctan{x}\right),
\]
which is the sought result.

\end{document}
