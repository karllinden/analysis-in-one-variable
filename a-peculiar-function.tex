\documentclass[a4paper]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}

\newcommand{\dx}{\mathrm{d}x}
\newcommand{\ddx}{\frac{\mathrm{d}}{\dx}}

\begin{document}

\title{A Peculiar Function \\ \large{Exercise 3.19}}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle
\vfill
\input{title-footer}
\clearpage
\null
\clearpage

\section*{A Peculiar Function}

\subsection*{Task}

Suppose that \((r_n)_{n=1}^\infty = \mathbb{Q}\). Prove that the function
\[ g(x) = \sum_{n \text{ s.t. } r_n < x} \frac{1}{n^2} \]
is continuous on \(\mathbb{R}\) and differentiable on \(x \notin \mathbb{Q}\).

\subsection*{Solution}
From now on
\[ \sum_{n \text{ s.t. } P(n)} g(n) \]
where $P$ is a statement with the free variable $n$ and $g$ is any function will
be written as
\[ \sum_{P(n)} g(n). \]
Thus
\[
  \sum_{n \text{ s.t. } r_n < x} \frac{1}{n^2} = \sum_{r_n < x} \frac{1}{n^2}.
\]

In fact \(f\) is \textbf{not} continuous on \(\mathbb{R}\). This can be shown
by the fact that
\[ \lim_{x \to a^+} f(x) \neq f(a), \quad a \in \mathbb{Q}. \]
If \(a \in \mathbb{Q}\) then there exists an \(r_m\) such that \(a = r_m\).
Consider the right neighbourhood of \(a\), that is \(a < x < a + \delta\) where
\(\delta > 0\). In this interval
\[
  f(x)
    = \sum_{r_n < x} \frac{1}{n^2}
    = \sum_{r_n < a} \frac{1}{n^2} + \sum_{a \leq r_n < x} \frac{1}{n^2}
    = f(a) + \sum_{a \leq r_n < x} \frac{1}{n^2}.
\]
But the term \(\frac{1}{m^2}\) is always in the sum, as \(a=r_m\) is always in
the interval \([a,a+\delta)\). Also the sum
\[ \sum_{a < r_n < x} \frac{1}{n^2} > 0 \]
since it is a sum of squares and \((a,x)\) is non-empty, so it must contain a
rational number.
Hence,
\[
  f(x) = f(a) + \frac{1}{m^2} + \sum_{a < r_n < x} \frac{1}{n^2}
    > f(a) + \frac{1}{m^2}.
\]
Let \(\epsilon \leq \frac{1}{m^2}\), say for example
\(\epsilon = \frac{1}{2m^2}\), then for all \(\delta > 0\) it holds that
\[
  |f(x) - f(a)| > \left|f(a) + \frac{1}{m^2} - f(a)\right| = \frac{1}{m^2}
  \geq \epsilon = \frac{1}{2m^2}.
\]
Since this, by definition, disproves
\[ \lim_{x \to a^+} f(x) = f(a) \]
\(f\) cannot be continuous on all of \(\mathbb{R}\), as
\(\mathbb{Q} \subset \mathbb{R}\) and \(f\) is not continuous on \(\mathbb{Q}\).
This also shows that \(f\) is not differentiable on \(\mathbb{Q}\) since
differentiability requires continuity.

However, \(f\) can still be continuous and differentiable on
\(x \notin \mathbb{Q}\).
Before going on with the proof a tool will be needed.

Let
\[ \phi_n = \sum_{k=n}^{\infty} \frac{1}{k^2}. \]
First note that \(\phi_n\) is a non-empty sum of squares so it is strictly
positive, that is \(\phi_n > 0\). Secondly, assuming \(n > 1\), the following
inequality holds
\[
  \phi_n
    = \sum_{k=n}^{\infty} \frac{1}{k^2}
    = \int_{n}^\infty \frac{1}{\lfloor x \rfloor^2} \dx
    < \int_{n}^\infty \frac{1}{(x-1)^2} \dx.
\]
This can be visualised using the plot below.
\begin{figure}[H]
  \centering
  \input{plots/319.tex}
\end{figure}
The right hand integral can be computed explicitly.
\[
  \begin{split}
    \int_n^\infty \frac{1}{(x-1)^2} \dx
      &= \lim_{t \to \infty} \int_n^t \frac{1}{(x-1)^2} \dx
       = \lim_{t \to \infty} \left[ -\frac{1}{x-1} \right]_n^t \\
      &= \lim_{t \to \infty} \left( -\frac{1}{t-1} + \frac{1}{n-1} \right)
       = \frac{1}{n-1}
  \end{split}
\]
Hence, it is known that
\[ 0 < \phi_n < \frac{1}{n-1} \]
and from the squeeze theorem the result
\[ \lim_{n \to \infty} \phi_n = 0 \]
is acquired.

Back to the original function, \(f\).
To show that it is continuous on \(\mathbb{R} \setminus \mathbb{Q}\), it is to
be proven that
\[ \lim_{x \to a} f(x) = f(a) \]
when \(a \notin \mathbb{Q}\).
This can be done by showing the one-sided limits
\[
  \lim_{x \to a^-} f(x) = f(a)
  \quad \text{and} \quad
  \lim_{x \to a^+} f(x) = f(a)
\]
Let \(\epsilon > 0\) be given.
For the left-sided limit it must be shown that
\[
  |f(x) - f(a)|
    = |f(a) - f(x) |
    = \left| \sum_{r_n < a} \frac{1}{n^2} - \sum_{r_n < x} \frac{1}{n^2} \right|
    = \left| \sum_{x \le r_n < a} \frac{1}{n^2} \right|
\]
can be made smaller than every given \(\epsilon\) and the same for the
right-sided limit,
\[
  |f(x) - f(a)|
    = \left| \sum_{r_n < x} \frac{1}{n^2} - \sum_{r_n < a} \frac{1}{n^2} \right|
    = \left| \sum_{a \le r_n < x} \frac{1}{n^2} \right|.
\]
Both can be shown simultaneously be choosing an appropriate neighbourhood
\(0 < |x-a| < \delta\).
Since \(|a - r_k| > 0\) for all \(k\), it is possible to choose a neighbourhood
around \(a\) that does not include \(r_1,r_2,\dotsc,r_{N-1}\).
Thus, let
\[ \delta = \min\{|a-r_1|, |a-r_2|, \dotsc, |a-r_{N-1}|\}. \]
Now the neighbourhood
\[ 0 < |x - a| < \delta \]
does not contain \(r_1, r_2, \dotsc, r_{N-1}\), which means that the
neighbourhood can only contain \(r_k\) from \(r_N, r_{N+1}, \dotsc\), but then
it is known that the above sums can only contain terms from
\[ \frac{1}{N^2}, \frac{1}{(N+1)^2}, \frac{1}{(N+2)^2}, \dotsc \]
Hence, both of the above sums are strictly less than \(\phi_N\), which means
\[ |f(x) - f(a)| < \phi_N. \]
By the definition of limit, \(\phi_N\) can be made smaller than any
\(\epsilon > 0\) by choosing a large enough \(N\).
Thus, it is possible to choose an \(N\) which yields a \(\delta\) which fulfills
the definition,
\[ 0 < |x-a| < \delta \quad \implies \quad |f(x) - f(a)| < \epsilon \]
which completes the proof of the continuity on
\(\mathbb{R} \setminus \mathbb{Q}\).

In general the function is not differentiable on all of
\(\mathbb{R} \setminus \mathbb{Q}\).
To show this let \(m\) be a natural number and let
\(c_1,c_2,\dots,c_{m-1} \notin \mathbb{Q}\) be fixed and consider the left-sided
derivative
\[
  \lim_{x \to c_k^-} \frac{f(c_k)-f(x)}{c_k - x}.
\]
It is possible to construct a sequence \((x_{k,i})_{i=1}^\infty\) so that
\[
  x_{k,i} = c_k - \frac{1}{i^3}
\]
for which it holds that
\[
  x_{k,i} < c_k \quad \text{and} \quad \lim_{i \to \infty} x_{k,i} = c_k
\]
which makes it possible to write the left-sided derivative as
\[
  \lim_{i \to \infty} \frac{f(c_k) - f(x_{k,i})}{\frac{1}{i^3}}
    = \lim_{i \to \infty} i^3 (f(c_k) - f(x_{k,i}))
    = \lim_{i \to \infty} i^3 \sum_{x_{k,i} \le r_n < c_k} \frac{1}{n^2}.
\]
It is possible to compose the sequence \((r_n)_{n=1}^\infty\) so that the
following inequality always holds.
\[
  x_{k,i} < r_{mi+k} < x_{k,i+1}
\]
This is possible since the sub-sequence \((r_{mj})_{j=1}^\infty\) can be an
enumeration of all rational numbers other than the ones bound under the above
constrain.
The inequality directly yields
\[ \sum_{x_{k,i} \le r_n < c_k} \frac{1}{n^2} > \frac{1}{(mi+k)^2}, \]
but this means the left-sided derivative is
\[
  \lim_{i \to \infty} i^3 \sum_{x_{k,i} \le r_n < c_k} \frac{1}{n^2}
    > \lim_{i \to \infty} \frac{i^3}{(mi+k)^2}
    = \infty
\]
and the derivative cannot exist at \(c_k\) although \(c_k \notin \mathbb{Q}\).

Note that the derivative cannot exist at any of the points
\(c_1,c_2,\dots,c_{m-1}\) and that \(m\) is an arbitrary natural number so it is
possible for \(f\) to not be differentiable on a countable subset of
\(\mathbb{R} \setminus \mathbb{Q}\).
However, it is hard to see whether or not it is possible to construct \(f\) so
that it is not differentiable at any \(x \in \mathbb{R}\), since \(\mathbb{R}\)
is uncountable.
In fact, the writer of this text has a vague feeling that this is not even
possible, and that it can be proven using some theorem of Lebesgue.

Another interesting problem is to determine if it is possible to construct
\((r_n)_{n=1}^\infty\) so that \(f\) is differentiable on all of
\(\mathbb{R} \setminus \mathbb{Q}\), or if \(f\) always have to have at least
one point at which it is not differentiable.
The investigation of this is left as an exercise to the reader.

\end{document}
