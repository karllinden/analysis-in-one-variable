\documentclass[a4paper]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{float}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}
\usepackage{pst-plot}
\usepackage{pstricks}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\renewcommand{\d}[1]{\mathrm{d}{#1}}
\newcommand{\dd}[1]{\frac{\mathrm{d}}{\d{#1}}}

\begin{document}

\title{Homework 10 \\ Analysis in one variable}
\author{
  Karl \textsc{Lind\'{e}n} \\
  <karl.linden.887@student.lu.se> \\
}
\maketitle
\vfill
\input{title-footer.tex}
\clearpage

\section*{Homework 10}

\begin{enumerate}[label=\textbf{(\alph*)}]
  \item
  It is known that
  \[
    \cos{x}
      =
      P_{2n}(x) + E_{2n+1}(x)
      =
      \sum_{k=0}^n (-1)^k \frac{x^{2k}}{(2k)!} + E_{2n+1}(x),
  \]
  where
  \[ |E_{2n+1}(x)| \le \frac{|x|^{2n+2}}{(2n+2)!}. \]
  Thus, by the linearity of the integral,
  \[
    \int_0^6 \cos{x} \d{x} =
      \int_0^6 P_{2n}(x) \d{x} + \int_0^6 E_{2n+1}(x)\d{x},
  \]
  and it is seen that \(\int_0^6 E_{2n+1}(x)\d{x}\) is the error when
  approximating
  \[ \int_0^6 \cos{x} \d{x} \approx \int_0^6 P_{2n}(x) \d{x}. \]
  Computing an error estimate yields, using the triangle inequality for integrals
  and the fact that \(x\) is non-negative,
  \[
    \begin{split}
      \left| \int_0^6 E_{2n+1}(x)\d{x} \right|
        &\le
        \int_0^6 |E_{2n+1}(x)| \d{x}
        \le
        \int_0^6 \frac{|x|^{2n+2}}{(2n+2)!} \d{x}
        \\
        &=
        \int_0^6 \frac{x^{2n+2}}{(2n+2)!} \d{x}
        =
        \left[ \frac{x^{2n+3}}{(2n+3)!} \right]_0^6 = \frac{6^{2n+3}}{(2n+3)!}.
    \end{split}
  \]
  Computing the error estimate for \(n = 7\) and \(n = 8\) yields
  \[
    \frac{708588}{14889875} > \frac{1}{100}
    \quad \text{and} \quad
    \frac{1417176}{282907625} \le \frac{1}{100},
  \]
  respectively.
  Hence, \(n = 8\) is the least \(n\) such that the error is guaranteed to be
  less than \(1/100\).
  This gives the polynomial
  \[ P_{16}(x) = \sum_{k=0}^8 (-1)^k \frac{x^{2k}}{(2k)!}. \]

  \item
  Computing the integral approximation for \(n = 8\) yields
  \[
    \begin{split}
      \int_0^6 \cos{x} \d{x}
        &\approx
        \int_0^6 P_{16}(x) \d{x}
        =
        \int_0^6 \sum_{k=0}^8 (-1)^k \frac{x^{2k}}{(2k)!} \d{x}
        \\
        &=
        \left[ \sum_{k=0}^8 (-1)^k \frac{x^{2k+1}}{(2k+1)!} \right]_0^6
        =
        \sum_{k=0}^8 (-1)^k \frac{6^{2k+1}}{(2k+1)!}.
    \end{split}
  \]
  An implementation of the iterative algorithm described later on gives the
  approximation
  \[ \int_0^6 \cos{x} \d{x} \approx -0.27481. \]
  Indeed, this is sufficiently close to the ``actual'' value of \(\sin(6)\),
  which is (approximately) \(-0.27942\).
  The difference between the values are \(0.00461 \le 0.01\), as expected.
\end{enumerate}

\section*{Optimization by iteration}

It is possible to rewrite the sum on the previous page as a (backwards)
iteration.
Let \((s_m)_{m=0}^n\) be the sequence defined by
\[ s_{m-1} = 1 - \frac{x^2}{2m(2m+1)} s_m, \quad s_n = 1. \]
In the last step when \(m = 0\), the desired result is that \(xs_0\) should be
equal to the result in (b) when \(n = 8\).
To prove that this is in fact the case, (backwards) induction is used.
It shall be proven that
\begin{equation}
  \tag{*}
  \label{eq:star}
  s_m = (2m+1)! \sum_{k=m}^n (-1)^{k-m} \frac{x^{2k-2m}}{(2k+1)!}.
\end{equation}
For \(m = n\) the formula holds, since
\[
  s_n
    =
    (2n+1)! \sum_{k=n}^n (-1)^{k-n} \frac{x^{2k-2n}}{(2k+1)!}
    =
    (2n+1)! (-1)^0 \frac{x^0}{(2n+1)!}
    =
    1,
\]
which satisfies the definition.
Assume \eqref{eq:star} holds for some \(m > 0\), then
\[
  \begin{split}
    s_{m-1}
      &=
      1 - \frac{x^2}{2m(2m+1)} s_m
      \\
      &=
      1 - \frac{x^2}{2m(2m+1)} (2m+1)!
        \sum_{k=m}^n (-1)^{k-m} \frac{x^{2k-2m}}{(2k+1)!}
      \\
      &=
      1 + (2m-1)! \sum_{k=m}^n (-1)^{k-m+1} \frac{x^{2k-2m+2}}{(2k+1)!}
      \\
      &=
      (2(m-1)+1)!
        \left(
          \frac{x^0}{(2(m-1)+1)!} + 
          \sum_{k=m}^n (-1)^{k-(m-1)} \frac{x^{2k-2(m-1)}}{(2k+1)!}
        \right)
      \\
      &=
      (2(m-1)+1)! \sum_{k=m-1}^n (-1)^{k-(m-1)} \frac{x^{2k-2(m-1)}}{(2k+1)!},
  \end{split}
\]
which proves that \eqref{eq:star} holds for \(m-1\).
By induction \eqref{eq:star} holds for all \(m\) such that \(0 \le m \le n\).
By inserting \(m = 0\), it is clear that
\[
  xs_0
    =
    x \sum_{k=0}^n (-1)^{k} \frac{x^{2k}}{(2k+1)!}
    =
    \sum_{k=0}^n (-1)^{k} \frac{x^{2k+1}}{(2k+1)!}.
\]

Even though what was done above might seem cumbersome and unnecessary, it allows
the polynomial to be evaluated in linear time, rather than in quadratic time,
which would be needed to evaluate the polynomial directly.
Also it reduces both round-off errors and the risk of overflow with higher
degree polynomials when performing numerical computations.
Lastly, although not very tempting, the iteration makes it feasible to perform
the computations by hand.

\end{document}
